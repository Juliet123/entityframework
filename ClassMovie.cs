﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntityFrameworkTask
{
    public class ClassMovie
    {
        public void MovieMethod()
        {
            using (var db = new Context())
            {
                Movie mymovie = new Movie { MovieID = 1, Name = "Merlin", Rank = "One", Year = "2008" };
                db.Movies.Add(mymovie);
                db.SaveChanges();
            }
        }
        

    }
}