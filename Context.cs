﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EntityFrameworkTask
{
    public class Context :DbContext
    {
        public DbSet<Movie> Movies { get; set; }
    }
}