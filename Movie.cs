﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntityFrameworkTask
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string Rank { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
    }
}